;
"use strict";

function DOMready() {

	// - Фиксируем шапку при прокрутке

	$(window).on("scroll", function() {
		fixedHeader();
	});

	var header = $(".header");
	var sticky = header.scrollTop();


	function fixedHeader() {
	  if ($(window).scrollTop() > sticky) {
	    header.addClass("sticky");
	  } else {
	    header.removeClass("sticky");
	  }
	}

	//- настройка открытия видео

	$('.js--play-video').magnificPopup({
		disableOn: 700,
		type: 'iframe',
		mainClass: 'mfp-fade',
		removalDelay: 160,
		preloader: false,
		fixedContentPos: true
	});

	$('.js--slider').slick ({
		slidesToShow: 3,
		slidesToScroll: 1,
		dots: false,
		prevArrow: $('.arrow .arrow__prev'),
		nextArrow: $('.arrow .arrow__next'),
		rows: false,
		responsive: [
			{
				breakpoint: 768,
					settings: {
					slidesToShow: 2,
				},
			},
			{
				breakpoint: 576,
					settings: {
					slidesToShow: 1,
				},
			},
		]
	});

	//- Открытие, закрытие формы при клике на кнопку

	$('body').on('click', '#openModal', function() {
		$('.js--modal').addClass('modal-active');
		$('html').addClass('block');
	});

	$('body').on('click', '.js--modal-close', function() {
		$('.js--modal').removeClass('modal-active');
		$('html').removeClass('block');
	});


	//- Кнопка меню в мобилке

	if(document.documentElement.clientWidth < 768) {
		$('body').on('click', '.js--open-menu', function(){
			$('html').toggleClass('block');
			$(this).toggleClass('header__mobile_active');
			$('.js--menu-active').slideToggle();
		});

		$('.js--menu-active').on('click', 'a[href^="#"]', function (event) {
			$('html').removeClass('block');
			$('.js--menu-active').slideToggle();
			$('.js--open-menu').toggleClass('header__mobile_active');
		});
	}

	//-Активный раздел меню

	$('.header').on('click', 'a[href^="#"]', function (event) {
		$('.menu__item_status_active').removeClass('menu__item_status_active');
		$(this).parent('li').addClass('menu__item_status_active');
	});

}

document.addEventListener("DOMContentLoaded", DOMready);